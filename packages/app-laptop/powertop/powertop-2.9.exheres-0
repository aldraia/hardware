# Copyright 2008, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}-v${PV}

SUMMARY="A tool for reducing power usage"
DESCRIPTION="
Computer programs can make your computer use more power. PowerTOP is a
Linux tool that helps you find those programs that are misbehaving while
your computer is idle.
"
HOMEPAGE="https://01.org/powertop"
DOWNLOADS="https://01.org/sites/default/files/downloads/${PN}/${MY_PNV}.tar.gz"

BUGS_TO="pioto@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( linguas: ca cs_CZ de_DE en_GB en_US es_ES hu_HU id_ID nl_NL zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18]
        virtual/pkg-config
    build+run:
        net-libs/libnl:=
        sys-apps/pciutils
        sys-libs/ncurses
        sys-libs/zlib
    suggestion:
        net-wireless/bluez [[ description = [ For disabling inactive bluetooth connections ] ]]
        net-wireless/wireless_tools [[ description = [ For enabling power-saving wireless settings ] ]]
        x11-apps/xrandr [[ description = [ For disabling TV output ] ]]
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
)

src_install() {
    default

    keepdir /var/cache/powertop
}

