# Copyright 2016 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

CL_HPP_VER=2.0.10
OCL12_COMPAT=2.1

require github [ user=KhronosGroup project=OpenCL-Headers tag=03490c3f8cb3b8667942b084a3151de20649f57a ]

SUMMARY="Khronos's OpenCL (Open Computing Language) header files"
HOMEPAGE="https://www.khronos.org/registry/cl"
DOWNLOADS+="
    https://github.com/KhronosGroup/OpenCL-CLHPP/releases/download/v${CL_HPP_VER}/cl2.hpp -> ${PNV}-cl2.hpp
    ${HOMEPAGE}/api/${OCL12_COMPAT}/cl.hpp -> ${PNV}-cl.hpp
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    built-against:
        x11-dri/mesa [[ note = [ cl_egl.h includes EGL/egl.h ] ]]
"

src_install() {
    insinto /usr/$(exhost --target)/include/CL
    doins CL/*.h

    # OpenCL C++ Bindings
    newins "${FETCHEDDIR}"/${PNV}-cl2.hpp cl2.hpp

    # OCL 1.2 compatibility
    newins "${FETCHEDDIR}"/${PNV}-cl.hpp cl.hpp

    # We're not interested in Direct3D things
    edo rm "${IMAGE}"/usr/$(exhost --target)/include/CL/cl_{dx9_media_sharing{,_intel},d3d10,d3d11}.h
}

